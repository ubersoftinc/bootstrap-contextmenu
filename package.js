Package.describe({
  name: 'ubersoft:bootstrap-contextmenu',
  version: '0.3.4',
  summary: 'Bootstrap Context Menu - Wrapped for Meteor',
  git: 'https://bitbucket.org/ubersoftinc/bootstrap-contextmenu.git',
  documentation: 'README.md'
})

Package.onUse(function(api) {
  api.addFiles(['bootstrap-contextmenu.js'], 'client')
})
